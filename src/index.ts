import 'babel-polyfill'
import { Wallet, utils } from 'ethers'
import { HandlersMap } from "./types"
import { call, replyMessage, replyError } from "./messaging"
import { EntityResolver, Gateway } from "dvote-js"
import { TextRecordKeys, TextListRecordKeys } from "dvote-js/dist/dvote/entity-resolver"

///////////////////////////////////////////////////////////////////////////////
// SETUP
///////////////////////////////////////////////////////////////////////////////

export function init(handlers: HandlersMap) {
  // Load on window.<*> so that functions can be invoked from the global scope
  for (let handlerName in handlers) {
    window[handlerName] = handlers[handlerName]
  }
  window["call"] = call
  window["replyMessage"] = replyMessage
  window["replyError"] = replyError
}

const handlers: HandlersMap = {
  generateMnemonic,
  mnemonicToAddress,
  mnemonicToPublicKey,
  fetchEntityMetadata,
  fetchEntityAllFields,
  fetchEntityTextField,
  fetchEntityTextList,
  fetchTextFile
}

init(handlers)

///////////////////////////////////////////////////////////////////////////////
// IMPLEMENTATIONS HERE
///////////////////////////////////////////////////////////////////////////////

/**
 * Generate a random set of mnemonic words to use for a new identity
 */
function generateMnemonic(): string {
  return utils.HDNode.entropyToMnemonic(utils.randomBytes(24))
}

/**
 * Compute the keypair that corresponds to the given mnemonic and extract its Ethereum address
 * @param mnemonic Seed phrase
 * @param hdPath Derivation path (optional)
 */
function mnemonicToAddress(mnemonic: string, hdPath: string = "m/44'/60'/0'/0/0"): Promise<string> {
  if (!mnemonic) throw new Error("Invalid mnemonic")
  const wallet = Wallet.fromMnemonic(mnemonic, hdPath)

  return wallet.getAddress()
}

/**
 * Compute the keypair that corresponds to the given mnemonic and return the public key
 * @param mnemonic Seed phrase
 * @param hdPath Derivation path (optional)
 */
function mnemonicToPublicKey(mnemonic: string, hdPath: string = "m/44'/60'/0'/0/0"): Promise<string> {
  if (!mnemonic) throw new Error("Invalid mnemonic")
  const wallet = Wallet.fromMnemonic(mnemonic, hdPath)

  wallet.getAddress() // ensure that the public key has been computed
  return wallet.signingKey.publicKey
}

/**
 * Fetches the JSON metadata of the given entity. The JSON data origin is determined from the Blockchain.
 * The data schema is expected to conform to the given schema: 
 * https://vocdoni.io/docs/#/architecture/components/entity?id=data-schema
 * @param resolverAddress Address of the EntityResolver contract
 * @param entityId ID of the entity
 * @param providerUrl URL of the Provider/Gateway to use
 */
async function fetchEntityMetadata(resolverAddress: string, entityId: string, wsGatewayUri: string, ethProviderUri: string): Promise<any> {
  const provider = Gateway.ethereumProvider(ethProviderUri)
  const EntityResolverFactory = new EntityResolver({ provider })
  const resolverInstance = EntityResolverFactory.attach(resolverAddress)

  const metadataOrigin = await resolverInstance.text(entityId, TextRecordKeys.JSON_METADATA_CONTENT_URI)
  if (!metadataOrigin) throw new Error("The entity does not seem have any metadata origin registered")

  const gw = new Gateway(wsGatewayUri)
  const jsonMetadata = (await gw.fetchFile(metadataOrigin)).toString()

  const decodedMetadata = JSON.parse(jsonMetadata)
  decodedMetadata.meta = metadataOrigin

  // ensure to disconnect
  if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
  gw.disconnect()

  return decodedMetadata
}

/**
 * Fetches the data field of the given entity on the blockchain
 * The valid keys are defined on https://vocdoni.io/docs/#/architecture/components/entity?id=resolver-keys
 * @param resolverAddress Address of the EntityResolver contract
 * @param entityId ID of the entity
 * @param providerUrl URL of the Provider/Gateway to use
 * @param key The Key to fetch
 */
async function fetchEntityTextField(resolverAddress: string, entityId: string, providerUrl: string, key: string): Promise<string> {
  const EntityResolverFactory = new EntityResolver({ providerUrl: providerUrl })
  const resolverInstance = EntityResolverFactory.attach(resolverAddress)

  let result
  switch (key) {
    // Need parsing
    case TextRecordKeys.LANGUAGES:
    case TextRecordKeys.GATEWAYS_UPDATE_CONFIG:
    case TextRecordKeys.ACTIVE_PROCESS_IDS:
    case TextRecordKeys.ENDED_PROCESS_IDS:
      result = JSON.parse(await resolverInstance.text(entityId, key))
      if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
      return result

    // Raw text
    case TextRecordKeys.NAME:
    case TextRecordKeys.JSON_METADATA_CONTENT_URI:
    case TextRecordKeys.VOTING_CONTRACT_ADDRESS:
    case TextRecordKeys.AVATAR_CONTENT_URI:
      result = resolverInstance.text(entityId, key)
      if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
      return result
  }

  // Language-dependent data
  if (key.match(new RegExp("^" + TextRecordKeys.NEWS_FEED_URI_PREFIX + "[a-z]{2}$"))) {
    result = resolverInstance.text(entityId, key)
    if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
    return result
  }
  else if (key.match(new RegExp("^" + TextRecordKeys.DESCRIPTION_PREFIX + "[a-z]{2}$"))) {
    result = resolverInstance.text(entityId, key)
    if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
    return result
  }

  // ensure to disconnect if using WS
  if (resolverInstance.provider.polling) resolverInstance.provider.polling = false

  throw new Error("The given key is not supported")
}

/**
 * Fetches the data list of the given entity on the blockchain
 * The valid keys are defined on https://vocdoni.io/docs/#/architecture/components/entity?id=resolver-keys
 * @param resolverAddress Address of the EntityResolver contract
 * @param entityId ID of the entity
 * @param providerUrl URL of the Provider/Gateway to use
 * @param key The Key to fetch
 */
async function fetchEntityTextList(resolverAddress: string, entityId: string, providerUrl: string, key: string): Promise<string[]> {
  const EntityResolverFactory = new EntityResolver({ providerUrl: providerUrl })
  const resolverInstance = EntityResolverFactory.attach(resolverAddress)

  let result
  switch (key) {
    // Need parsing
    case TextListRecordKeys.BOOT_ENTITIES:
    case TextListRecordKeys.CENSUS_SERVICE_SOURCE_ENTITIES:
    case TextListRecordKeys.FALLBACK_BOOTNODE_ENTITIES:
    case TextListRecordKeys.GATEWAY_BOOT_NODES:
    case TextListRecordKeys.RELAYS:
    case TextListRecordKeys.TRUSTED_ENTITIES:
      result = (await resolverInstance.list(entityId, key)).map(entry => JSON.parse(entry || "{}"))
      if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
      return result

    // Raw text
    case TextListRecordKeys.CENSUS_IDS:
    case TextListRecordKeys.CENSUS_MANAGER_KEYS:
    case TextListRecordKeys.CENSUS_SERVICES:
      result = resolverInstance.list(entityId, key)
      if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
      return result

    default:
      if (resolverInstance.provider.polling) resolverInstance.provider.polling = false
      throw new Error("The given key is not supported")
  }
}

/**
 * Fetches the data fields of the given entity on the blockchain and returns a key-value 
 * object with the keys defined on https://vocdoni.io/docs/#/architecture/components/entity?id=resolver-keys
 * Note: Calling fetchEntity() triggers many more requests than calling fetchEntityMetadata() and may be slower
 * @param resolverAddress Address of the EntityResolver contract
 * @param entityId ID of the entity
 * @param providerUrl URL of the Provider/Gateway to use
 */
async function fetchEntityAllFields(resolverAddress: string, entityId: string, providerUrl: string): Promise<any> {
  const EntityResolverFactory = new EntityResolver({ providerUrl: providerUrl })
  const resolverInstance = EntityResolverFactory.attach(resolverAddress)

  const result = {
    // STRINGS
    [TextRecordKeys.LANGUAGES]: JSON.parse(await resolverInstance.text(entityId, TextRecordKeys.LANGUAGES)),
    [TextRecordKeys.NAME]: await resolverInstance.text(entityId, TextRecordKeys.NAME),
    [TextRecordKeys.JSON_METADATA_CONTENT_URI]: await resolverInstance.text(entityId, TextRecordKeys.JSON_METADATA_CONTENT_URI),
    [TextRecordKeys.VOTING_CONTRACT_ADDRESS]: await resolverInstance.text(entityId, TextRecordKeys.VOTING_CONTRACT_ADDRESS),
    [TextRecordKeys.GATEWAYS_UPDATE_CONFIG]: JSON.parse(await resolverInstance.text(entityId, TextRecordKeys.GATEWAYS_UPDATE_CONFIG)),
    [TextRecordKeys.ACTIVE_PROCESS_IDS]: JSON.parse(await resolverInstance.text(entityId, TextRecordKeys.ACTIVE_PROCESS_IDS)),
    [TextRecordKeys.ENDED_PROCESS_IDS]: JSON.parse(await resolverInstance.text(entityId, TextRecordKeys.ENDED_PROCESS_IDS)),
    [TextRecordKeys.AVATAR_CONTENT_URI]: await resolverInstance.text(entityId, TextRecordKeys.AVATAR_CONTENT_URI),

    // STRING ARRAYS
    [TextListRecordKeys.BOOT_ENTITIES]: (await resolverInstance.list(entityId, TextListRecordKeys.BOOT_ENTITIES)).map(entry => JSON.parse(entry || "{}")),
    [TextListRecordKeys.CENSUS_IDS]: await resolverInstance.list(entityId, TextListRecordKeys.CENSUS_IDS),
    [TextListRecordKeys.CENSUS_MANAGER_KEYS]: await resolverInstance.list(entityId, TextListRecordKeys.CENSUS_MANAGER_KEYS),
    [TextListRecordKeys.CENSUS_SERVICES]: await resolverInstance.list(entityId, TextListRecordKeys.CENSUS_SERVICES),
    [TextListRecordKeys.CENSUS_SERVICE_SOURCE_ENTITIES]: (await resolverInstance.list(entityId, TextListRecordKeys.CENSUS_SERVICE_SOURCE_ENTITIES)).map(entry => JSON.parse(entry || "{}")),
    [TextListRecordKeys.FALLBACK_BOOTNODE_ENTITIES]: (await resolverInstance.list(entityId, TextListRecordKeys.FALLBACK_BOOTNODE_ENTITIES)).map(entry => JSON.parse(entry || "{}")),
    [TextListRecordKeys.GATEWAY_BOOT_NODES]: (await resolverInstance.list(entityId, TextListRecordKeys.GATEWAY_BOOT_NODES)).map(entry => JSON.parse(entry || "{}")),
    [TextListRecordKeys.RELAYS]: (await resolverInstance.list(entityId, TextListRecordKeys.RELAYS)).map(entry => JSON.parse(entry || "{}")),
    [TextListRecordKeys.TRUSTED_ENTITIES]: (await resolverInstance.list(entityId, TextListRecordKeys.TRUSTED_ENTITIES)).map(entry => JSON.parse(entry || "{}")),
  }

  // language dependent fields
  for (let lang of result[TextRecordKeys.LANGUAGES]) {
    result[TextRecordKeys.NEWS_FEED_URI_PREFIX + lang] = await resolverInstance.text(entityId, TextRecordKeys.NEWS_FEED_URI_PREFIX + lang)
    result[TextRecordKeys.DESCRIPTION_PREFIX + lang] = await resolverInstance.text(entityId, TextRecordKeys.DESCRIPTION_PREFIX + lang)
  }

  // ensure to disconnect if using WS
  if (resolverInstance.provider.polling) resolverInstance.provider.polling = false

  return result
}

/**
 * Fetches the file referenced by a Content URI
 * @param contentUri The Content URI to attempt to pull the data from. See http://vocdoni.io/docs/#/architecture/protocol/data-origins?id=content-uri
 */
async function fetchTextFile(contentUri: string, gatewayWsUri: string): Promise<string> {
  if (!contentUri) throw new Error("Invalid contentUri")
  else if (!gatewayWsUri) throw new Error("Invalid gatewayWsUri")

  const gw = new Gateway(gatewayWsUri)
  const data = await gw.fetchFile(contentUri)
  const result = data.toString()
  gw.disconnect()
  return result
}

///////////////////////////////////////////////////////////////////////////////
// HELPERS
///////////////////////////////////////////////////////////////////////////////

// let needsReconnect = false

// function checkWeb3Connection() {
//   return new Promise(resolve => {
//     if (needsReconnect) {
//       // emitEvent("disconnected")
//       web3.setProvider(BLOCKCHAIN_PROVIDER_URL)
//       needsReconnect = false
//       setTimeout(resolve, 10)
//     }
//     else {
//       resolve()
//     }
//   })
// }

import { JS_REQUEST_RESPONSE_CHANNEL, JS_EVENTS_CHANNEL } from "./settings"

///////////////////////////////////////////////////////////////////////////////
// INCOMING CALLS
///////////////////////////////////////////////////////////////////////////////

export function call(expression: any): Promise<any> {
  if (typeof expression == "function") return Promise.resolve().then(() => expression())
  else return Promise.resolve(expression)
}

///////////////////////////////////////////////////////////////////////////////
// OUTGOING MESSAGES
///////////////////////////////////////////////////////////////////////////////

export function replyMessage(id: number, response?: any) {
  window["flutter_inappbrowser"].callHandler(JS_REQUEST_RESPONSE_CHANNEL, { id, error: false }, response)
  return null
}
export function replyError(id: number, error?: Error) {
  window["flutter_inappbrowser"].callHandler(JS_REQUEST_RESPONSE_CHANNEL, { id, error: true }, error && error.message || error)
  return null
}
export function emitEvent(type: string, payload: any) {
  window["flutter_inappbrowser"].callHandler(JS_EVENTS_CHANNEL, type, payload)
  return null
}

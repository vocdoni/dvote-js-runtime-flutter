# DVote JS runtime for Flutter

The purpose of this component is to overcome the current limitations of the ecosystem available to apps running on Flutter.

The component provides access to a full fledged JS runtime, that will only run explicitly bundled static HTML/JS code.

This is intended as a workaround until Dart has full support for cryptographic and blockchain libraries.

## Usage
Check the exampe on `example/*`.

Within your Flutter code, invoke the available runtime functions as follows.

### Add a listener

```dart
// listen for post messages coming from the JavaScript side
this.webViewController.addJavaScriptHandler(
    "JS_REQUEST_RESPONSE_CHANNEL", (arguments) => onMessageReceived(arguments));
```

### Trigger an action

```dart
final requestId = 1234;
final jsExpression = "myJavascriptFunction('param1', { param2: true }, false)";

await this.webViewController.injectScriptCode('''
    call(() => $jsExpression)
    .then(result => replyMessage($requestId, result))
    .catch(error => replyError($requestId, error))
''');
```

### Handle a response
```dart
  onMessageReceived(List<dynamic> arguments) async {
    print(arguments);

    // Expected:
    //  arguments[0] = { id: <int>, error: <bool> }
    //  arguments[1] = <data>
    
    // ...
  }
```

## Available methods

From Dart, you can construct JS expressions that make use of these primitives:

```typescript
function generateMnemonic(): string
function mnemonicToAddress(mnemonic: string, hdPath: string = "m/44'/60'/0'/0/0"): Promise<string>
function mnemonicToPublicKey(mnemonic: string, hdPath: string = "m/44'/60'/0'/0/0"): Promise<string>
function fetchEntityMetadata(resolverAddress: string, entityId: string, wsGatewayUri: string, ethProviderUri: string): Promise<EntityMetadata>
function fetchEntityTextField(resolverAddress: string, entityId: string, providerUrl: string, key: string): Promise<string>
function fetchEntityTextList(resolverAddress: string, entityId: string, providerUrl: string, key: string): Promise<string[]>
function fetchEntityAllFields(resolverAddress: string, entityId: string, providerUrl: string): Promise<EntityFields>
```

## Customize

* Add your new handler on `index.ts`
* Declare the function within `const handlers: HandlersMap = { ... }`
* Run `npm run build`
* Use `dist/runtime.html`

## Future work

- [X] Provide a dart example
- [ ] Provide a dart package to import
- [ ] Publish the repo as a package

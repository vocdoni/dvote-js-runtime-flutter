const fs = require("fs")
const bundleOrigin = "./dist/index.js"
const bundleDestination = "./dist/runtime.html"

if (!fs.existsSync(bundleOrigin)) {
  console.error("Error: The bundle file does not exist", bundleOrigin)
  console.error("Run 'npm run build' to compile the bundle into a JSON file")
  process.exit(1)
}

const bundle = fs.readFileSync(bundleOrigin).toString()

const html = `<!DOCTYPE html><html><head><meta charset="utf-8"></head><body><script>${bundle}</script></body></html>`

fs.writeFileSync(bundleDestination, html)
// fs.writeFileSync("./dist/index.json", JSON.stringify({ html }))
